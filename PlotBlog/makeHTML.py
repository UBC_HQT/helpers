#!/env/python
import glob,os,sys,re,codecs
from html import HTML
from sys import version_info
from HTMLParser import HTMLParser

#SETUP
host="http://atlas-t3-ubc.westgrid.ca/UBC/"
analysisFolder = "HQTweb"
hostlocation = "/data/www/"
py3 = version_info[0] > 2 #creates boolean value for test that Python major version > 2
if py3:
  response = input("INFO :: Name your study (one string) >> : ")
else:
  response = raw_input("INFO :: Name your study (one string) [MUST] >> : ")

if response is "":
    print "You provided no name for your study"
    print "INFO :: Exiting ..."
    sys.exit()
else:
    studyName = response

user = raw_input("Type in your name (Are you alison, matthias, steffen or robin? ) [MUST] >> : ")


userList = set(['Alison','Matthias','Steffen','Robin',
                'alison','matthias','steffen','robin'])
if user not in userList:
    print "User is not in user list, sorry"
    print "INFO :: Exiting ..."
    sys.exit()

choice = raw_input("Do you want to update your navigation (link to your study)? [MUST] [y/n] >> : ").lower()
yes = set(['yes','y', 'ye', 'YES','Yes','j','Ja','ja'])
no = set(['no','n','NO','No'])
if choice in yes:
   NavUpdate = True
elif choice in no:
   NavUpdate = False
else:
    print "Please respond with 'yes' or 'no'"
    print "INFO :: Exiting ..."
    sys.exit()



if "alison" in user:
    email = "alison.lister@cern.ch"
elif "matthias" in user:
    email = "matthias.danninger@cern.ch"
elif "steffen" in user:
    email = "steffen.henkelmann@cern.ch"
elif "robin" in user:
    email = "robin.newhouse@cern.ch"
else:
    email = "<user>@cern.ch"
    
analysis = ['VLQ','WbXWbX']

h = HTML()

def printHeader():
    study = open(studyName + '.html', 'w')
    print >> study.write("""<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>UBC top particle physics | Portfolio :: w3layouts</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Your Trip Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script src="../js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<!-- pop-up-script -->
<script src="../js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="../css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('.img-top a').Chocolat();
		});
		</script>
<!-- //pop-up-script -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="../js/move-top.js"></script>
<script type="text/javascript" src="../js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smooth-scrolling -->
</head>
    """)
    study.close()


def printBannerBeg():
    with open(studyName+'.html', "a") as f:
        f.write("""<body>
<!-- banner -->
<div class="banner">
<div class="container">
<div class="banner_top">
<div class="banner_top_left">
  <p>The top particle physics
  team @UBC</p>
</div>
<div class="banner_top_right">
  <form>
  <input type="text" value="Search Here..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}" required="">
  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
  </form>
</div>
<div class="clearfix"> </div>
</div>
<nav class="navbar navbar-default">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  </button>
</div>
        """)
        f.close()

def printBannerEnd():
    with open(studyName+'.html', "a") as f:
        f.write("""
        </nav>
<div class="logo">
  <a href="../index.html">"""+analysis[0]+"""<span>"""+analysis[1]+"""</span></a>
</div>
<div class="dummy_text">
  <p></p>
</div>

</div>
</div>
<!-- //banner -->
""")
        f.close()
def printPortfolio():
    with open(studyName+'.html', "a") as f:
        f.write("""<!-- portfolio -->
<div class="portfolio">
<div class="container">
  <h3>"""+studyName+"""</h3>
<div class="portfolio-grids">
<div class="sap_tabs">			
<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
	  <ul class="resp-tabs-list">
        """)
        f.close()

def printNav(pages):
    with open(studyName+'.html', "a") as f:
        f.write("""<li class="resp-tab-item"><span>"""+pages+"""</span></li>
        """)
        f.close()

def EndPrintNav():
    with open(studyName+'.html', "a") as f:
        f.write("""</ul>

        """)
        f.close()
        
def preparePlotPage():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<div class="resp-tabs-container">
	<div class="tab-1 resp-tab-content">								
	<div class="main">
        """)
        f.close()


def printPlot(subdir, plot):
    d_plot = os.path.splitext(plot)
    with open(studyName+'.html', "a") as f:
        f.write("""<div class="view">
	<div class="img-top">
	  <a href='"""+studyName+"""/"""+subdir+"""/"""+plot+"""' rel="Download as <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".pdf'>.pdf</a> | <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".png'>.png</a> | <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".root'>.root</a>" class="b-link-stripe b-animate-go  thickbox">
	  <img src='"""+studyName+"""/"""+subdir+"""/"""+plot+"""' class="img-responsive" alt="" />
	<div class="mask"></div>
	<div class="content">
	  <span class="info" title="extra information if needed" </span>
	</div>
	</a>
	</div>
	</div>
        """)
        f.close()

def EndPlotPage():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<div class="clearfix"> </div>
	</div>																					 	        					 
	</div>
    </div>
        """)
        f.close()

        

def PrintEndPortfolio():
    with open(studyName+'.html', "a") as f:
        f.write("""	
		</div>
	</div>
	</div>
    	<script src="../js/easyResponsiveTabs.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function () {
	$('#horizontalTab').easyResponsiveTabs({
	type: 'default', //Types: default, vertical, accordion           
	width: 'auto', //auto or any width like 600px
	fit: true   // 100% fit in a container
	});
	});
	
	</script>
	</div>
	</div>
	</div>
<!-- //portfolio -->
        """)
        f.close()


def PrintContact():
    with open(studyName+'.html', "a") as f:
        f.write("""	
<!-- contact -->
	<div class="contact" id="contact">
	  
	<div class="container">
	  <h3>Contact Me</h3>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
	  <h4>Address</h4>
	  <p>6224 Agricultural
	  Road<span>Vancouver, Canada</span></p>
	</div>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
	  <h4>Mail</h4>
	  <a href='mailto:"""+email+"""'>"""+email+"""</a>
	</div>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
	  <h4>Phone</h4>
	  <p>...</p>
	</div>
	<div class="clearfix"> </div>
	<!-- footer -->
	<div class="footer-copy">
	  <p>&copy 2016 Your Trip. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts.</a></p>
	</div>
	</div>
	</div>
	<!-- //contact -->
        """)
        f.close()

def EndFile():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
	$(document).ready(function() {
	/*
	var defaults = {
	containerID: 'toTop', // fading element id
	containerHoverID: 'toTopHover', // fading element hover id
	scrollSpeed: 1200,
	easingType: 'linear' 
	};
	*/
	
	$().UItoTop({ easingType: 'easeOutQuart' });
	
	});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	</body>
	</html>
        """)
        f.close()
def printBannerNav(user):
    with open(studyName+'.html', "a") as f:
        if "alison" in user:
            f.write("""
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html"class="active">Alison</a></li>
            <li><a href="../matthias.html">Matthias</a></li>
            <li><a href="../steffen.html">Steffen</a></li>
            <li><a href="../robin.html" >Robin</a></li>
            <li><a href="#contact" class="scroll">Contact Us</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()
        elif "matthias" in user:
            f.write("""
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html">Alison</a></li>
            <li><a href="../matthias.html"class="active">Matthias</a></li>
            <li><a href="../steffen.html">Steffen</a></li>
            <li><a href="../robin.html" >Robin</a></li>
            <li><a href="#contact" class="scroll">Contact Us</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()
    
        elif "steffen" in user:
            f.write("""
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html">Alison</a></li>
            <li><a href="../matthias.html">Matthias</a></li>
            <li><a href="../steffen.html"class="active">Steffen</a></li>
            <li><a href="../robin.html" >Robin</a></li>
            <li><a href="#contact" class="scroll">Contact Us</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()
        elif "robin" in user:
            f.write("""
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html">Alison</a></li>
            <li><a href="../matthias.html">Matthias</a></li>
            <li><a href="../steffen.html">Steffen</a></li>
            <li><a href="../robin.html" class="active">Robin</a></li>
            <li><a href="#contact" class="scroll">Contact Us</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()
        else:
            f.write("""
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html">Alison</a></li>
            <li><a href="../matthias.html">Matthias</a></li>
            <li><a href="../steffen.html">Steffen</a></li>
            <li><a href="../robin.html">Robin</a></li>
            <li><a href="#contact" class="scroll">Contact Us</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()

        
def makeHTML_init():
    printHeader()
    printBannerBeg()
    printBannerNav(user)
    printBannerEnd()
    printPortfolio()


def makeHTML(subdir, allPlots):
    preparePlotPage()
    for plot in allPlots:
        plot = plot.lstrip('./')
        printPlot(subdir, plot)
        #FillIndex_organisePlotsInTable(index,plot,nPlots)
#    FillIndex_organisePlotsInTableEnd(index)
    EndPlotPage()



def makeHTML_finalize():
    PrintEndPortfolio()
    PrintContact()
    EndFile()

def produceHTML():
    loc = os.getcwd()
    makeHTML_init()
    allSubDirs = os.walk('.').next()[1]
    for subdir in allSubDirs:
        if str(studyName) in str(subdir):
            continue
        else:
            printNav(subdir)
            
    EndPrintNav()
    for subdir in allSubDirs:
        os.chdir(subdir)
        nPlots = os.walk('.').next()[2]
#        print nPlots
        allPNG = glob.glob("./*.png")
#        print 'In subdir : '+str(subdir)+' there are '+str(nPlots)+' namely : '+str(allPNG)
        os.chdir(loc)
        if not str(studyName) in str(subdir):
            os.system("cp -r " + subdir + " " + studyName)
        makeHTML(subdir, allPNG)
    makeHTML_finalize()
#    for png in allPNG:
    #    print png


def upload():
    print "INFO :: Will now upload the files to the group webpage for you, " + user
    os.system("cp " + studyName + ".html " + hostlocation + "/" + analysisFolder + "/" + user + "/.")
    os.system("cp -r " + studyName + " " +hostlocation + "/"+ analysisFolder + "/" + user + "/.")       
    
def changeNavigation(user):
    print "INFO :: Will update your navigation and link to the new study"
    respTitle = raw_input("Please provide a name of your study (e.g. ttbar control plots) >> : ")
    if respTitle is "":
        print "You provided no name for your study, so the computer chose one"
        NameOfStudy = "I was too lazy to enter a study name"
    else:
        NameOfStudy = respTitle
        
    respContext = raw_input("Please provide a minimal description (e.g. which AT version used, selection) >> : ")
    if respContext is "":
        print "You provided no name for your study, so the computer chose one"
        context = "I was too lazy to provide study context"
    else:
        context = respContext

    print '###########################################'
    print "# Name of your study: " + NameOfStudy
    print '--------------------------------------------------'
    print "# Context : "
    print "# " + context
    print '###########################################'    


    stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-->"""
    with codecs.open(hostlocation + "/" + analysisFolder + "/" + user + '.html','r',encoding='utf8') as f:
        f_html = f.read()
        replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
<p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href="steffen/"""+studyName+""".html">here</a>
</div>
</div>
</div>
<div class="clearfix"> </div><!--ForExternalPython-->"""
        new_html = re.sub(stringToReplace, replacement, f_html)
        f.close()
#        os.system("rm "+user+".html")
    with codecs.open(hostlocation + "/" + analysisFolder + "/" + user + '.html','w',encoding='utf8') as nf:
        nf.write(new_html)
        nf.close()


        
if __name__ == "__main__":
    os.system("mkdir -p " + studyName)
    produceHTML()
    print 'INFO :: Will upload your study'
    upload()
    if NavUpdate:
      print 'INFO :: Will update the navigation and add a study to your page ...'
      changeNavigation(user)
    else:
      print 'INFO :: Will only provide and print the URL under which you can find the plots,'
      print '        No link from your main navigation will be provided ...'
      print "         >>> " + host + analysisFolder + "/" + user + "/" + studyName + ".html"
      
