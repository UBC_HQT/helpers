#!/bin/bash
if ([ "$#" != 1 ]); then
    echo "Usage: InitialSetup.sh <AnalysisTop version> ";
    echo "Example: InitialSetup.sh 2.3.41";
    kill -SIGINT $$;
  fi


mkdir AnalysisTop-$1
cd AnalysisTop-$1
export CERN_ACCOUNT=$USER # put your CERN username
export RUCIO_ACCOUNT=$CERN_ACCOUNT

setupATLAS
lsetup panda
lsetup rucio
lsetup pyami

rcSetup -u

rcSetup Top,$1
rc find_packages
rc compile

cp ../setup_scripts/quickSetup.sh .


cp ../setup_scripts/getAdditionalPackagesAndCompile.sh .
cp ../setup_scripts/getPyMiniAna.py .

cp ../setup_scripts/GETSTUFF.sh .