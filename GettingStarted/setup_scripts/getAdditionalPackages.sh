#!/bin/bash
export ANALYSISTOPCURRENT=$PWD
HQTVLQWbXTools=00-04-02
xAODJetReclustering=v3.141
BoostedJetTaggers=00-00-07
get_analysis_VLQ ${HQTVLQWbXTools} ${xAODJetReclustering} ${BoostedJetTaggers}