#! /bin/bash

. getAdditionalPackagesAndCompile.sh trunk v3.141 00-00-07 00-03-73

#github.com/demarley/PyMiniAna
. getPyMiniAna.py v2.0.0

#TopNTupleAnalysis
#svn co $SVNOFF/PhysicsAnalysis/TopPhys/xAOD/TopNtupleAnalysis/trunk TopNtupleAnalysis
git clone https://gitlab.cern.ch/UBC_HQT/vlq_wbx_wbx.git
svn co $SVNOFF/PhysicsAnalysis/TopPhys/TopPhysUtils/TopDataPreparation/trunk TopDataPreparation