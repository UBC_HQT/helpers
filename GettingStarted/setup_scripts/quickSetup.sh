#!/bin/bash
export CERN_ACCOUNT=${USER} # put your CERN username
export RUCIO_ACCOUNT=$CERN_ACCOUNT
export ANALYSISFOLDER=$PWD/TopNtupleAnalysis
setupATLAS
lsetup panda
lsetup rucio
lsetup pyami

rcSetup