#!/bin/bash


if ([ "$#" != 4 ]); then
    echo "Usage: getAdditional... <HQTVLQWbXTools tag> <JetClustering tag> <BoostedJetTaggers> <TopObjectSelection>";
    echo "Example: getAdditional... 00-04-03 v3.141 00-00-07 00-03-73";
                kill -SIGINT $$;
fi

HQTVLQWbXTools=$1
xAODJetReclustering=$2
BoostedJetTaggers=$3
TopObjectSelectionTools=$4

export ANALYSISTOPCURRENT=$PWD
cd $ANALYSISTOPCURRENT
if [ ${HQTVLQWbXTools} == "trunk" ];then
    echo 'Downloading the trunk'
    svn co $SVNTOP/Physics/Top/PhysAnalysis/Run2/HQTVLQWbXTools/trunk HQTVLQWbXTools
else
    echo "Downloading tag ${HQTVLQWbXTools}"
    svn co $SVNTOP/Physics/Top/PhysAnalysis/Run2/HQTVLQWbXTools/tags/HQTVLQWbXTools-${HQTVLQWbXTools} HQTVLQWbXTools
fi

git clone https://github.com/kratsg/xAODJetReclustering.git
cd xAODJetReclustering
git checkout tags/${xAODJetReclustering}
cd ..
# Bug fix for cluster cleaning:
#rc checkout_pkg TopObjectSelectionTools-${TopObjectSelection}
svn co $SVNROOT/PhysicsAnalysis/TopPhys/xAOD/TopObjectSelectionTools/tags/TopObjectSelectionTools-${TopObjectSelectionTools} TopObjectSelectionTools
svn co $SVNROOT/Reconstruction/Jet/BoostedJetTaggers/tags/BoostedJetTaggers-${BoostedJetTaggers} BoostedJetTaggers

# find packages and compile
rc find_packages
rc compile