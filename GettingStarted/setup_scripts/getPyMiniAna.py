#!/bin/bash

if ([ "$#" != 1 ]); then
    echo "Usage: getPyMiniAna.sh <PyMiniAna tag>";
    echo "Example: getPyMiniAna.sh v2.0.0";
                kill -SIGINT $$;
fi

PyMiniAna=$1

export ANALYSISTOPCURRENT=$PWD

cd $ANALYSISTOPCURRENT
git clone https://github.com/demarley/PyMiniAna.git
cd PyMiniAna
git checkout tags/${PyMiniAna}
cd ..
